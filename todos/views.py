from django.shortcuts import render, get_object_or_404
from todos.models import TodoList, TodoItem
from django.urls import reverse
from django.views.generic.edit import CreateView, UpdateView, DeleteView

# Create your views here.


def todo_list_view(request):
    todo_lists = TodoList.objects.all()
    context = {"todo_lists": todo_lists}
    return render(request, "todos/todo_list.html", context)


def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    todo_items = todo_list.items.all()
    context = {"todo_list": todo_list, "todo_items": todo_items}
    return render(request, "todos/todo_list_detail.html", context)


class TodoListCreateView(CreateView):
    model = TodoList
    fields = ["name"]
    template_name = "todos/todo_list_create.html"

    def get_success_url(self):
        return reverse("todos:todo_list_detail", args=[self.object.pk])


class TodoListUpdateView(UpdateView):
    model = TodoList
    fields = ["name"]
    template_name = "todos/todo_list_update.html"

    def get_success_url(self):
        return reverse("todos:todo_list_detail", kwargs={"id": self.object.pk})


class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/todo_list_delete.html"

    def get_success_url(self):
        return reverse("todos:todo_list_view")


class TodoItemView(CreateView):
    model = TodoItem
    fields = [
        "task",
        "due_date",
        "is_completed",
        "list",
    ]
    template_name = "todos/todo_item_create.html"

    def get_success_url(self):
        return reverse("todos:todo_list_view")


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    fields = ["task", "due_date", "is_completed", "list"]
    template_name = "todos/todo_item_update.html"

    def get_success_url(self):
        return reverse(
            "todos:todo_list_detail", kwargs={"id": self.object.list.id}
        )

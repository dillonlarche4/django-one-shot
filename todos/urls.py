from django.urls import path
from . import views

app_name = "todos"

urlpatterns = [
    path("", views.todo_list_view, name="todo_list_view"),
    path(
        "todo_list/<int:id>/", views.todo_list_detail, name="todo_list_detail"
    ),
    path(
        "create/", views.TodoListCreateView.as_view(), name="todo_list_create"
    ),
    path(
        "<int:pk>/edit/",
        views.TodoListUpdateView.as_view(),
        name="todo_list_update",
    ),
    path(
        "<int:pk>/delete/",
        views.TodoListDeleteView.as_view(),
        name="todo_list_delete",
    ),
    path(
        "items/create/", views.TodoItemView.as_view(), name="todo_item_create"
    ),
    path(
        "items/<int:pk>/edit/",
        views.TodoItemUpdateView.as_view(),
        name="todo_item_update",
    ),
]

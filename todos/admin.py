from django.contrib import admin
from .models import (
    TodoList,
    TodoItem,
)  # Assuming the TodoList model is in the same app's "models.py"


@admin.register(TodoList)
class TodoListAdmin(admin.ModelAdmin):
    list_display = ("id", "name")


@admin.register(TodoItem)
class TodoItem(admin.ModelAdmin):
    list_display = ("task", "due_date")


# Register your models here.
